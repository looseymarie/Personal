# My Personal Website | Gitlab
Welcome to the code for my personal website! My website is mainly finished.



# To Do

* [ ] Use javascript for the user name display

* [ ] Fix the progress bar css

* [ ] Move the secret code to a differant place

* [ ] Add info to the About tab

* [ ] Upload media and add it to the media tab

* [ ] Add settings reset button

* [ ] Add a local storage delete button

* [x] Fix the color set not saving or setting when you reload

* [x] Fix the l and k characters not decoding on the secret code
* There was an error with the ' and ’ (k) and the " and ” (l)

* [x] Use JavaScript to make lists easier to organize 
* I've mostly got this done, just need to add the rest of the variables and copy and paste a lot of things.

* [ ] Fix button padding on the left and right

* [ ] Redesign the input boxes
* Honestly, the default style looks ugly, and my style, was me playing with it, looks terrable.

* [x] Use textarea for secret code inputs
* Done! it looks a little better. There is more design to do, but i will do it later.

* [x] Make the tabs stay on what one was selected when you reload

# Suggestions
Suggestions and help would be awsome!